import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Login from './screens/Login'
import List from './screens/List'
import Book from './screens/Book'

const AppNavigator = createStackNavigator({
  Login: { screen: Login, navigationOptions: { header: null } },
  List: { screen: List, navigationOptions: { header: null } },
  Book: { screen: Book, navigationOptions: { header: null } }
})

export default createAppContainer(AppNavigator)